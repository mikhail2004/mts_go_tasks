package config

import (
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	Address string `yaml:"address"`
}

func NewConfig(configPath string) (cfg *Config, err error) {
	bytes, err := os.ReadFile(configPath)
	if err != nil {
		return
	}
	cfg = &Config{}
	err = yaml.Unmarshal(bytes, cfg)
	if err != nil {
		return nil, err
	}
	return
}
