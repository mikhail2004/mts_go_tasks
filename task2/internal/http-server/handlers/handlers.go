package handlers

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"math/rand"
	"net/http"
	"time"
)

func GetAPIver(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("v0.0.1"))
}

func ProcessHardRequest(w http.ResponseWriter, _ *http.Request) {
	if rand.Intn(2) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	time.Sleep(time.Duration(10+rand.Intn(10)) * time.Second)
}

type Request struct {
	InputString string `json:"inputString"`
}

type Response struct {
	Status        string `json:"status"`
	Error         error  `json:"error"`
	DecodedString string `json:"outputString"`
}

func HandleDecodeRequest(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		return
	}

	var req Request
	err = json.Unmarshal(reqBody, &req)
	if err != nil {
		return
	}

	decodedBytes, err := base64.StdEncoding.DecodeString(req.InputString)
	decodedString := string(decodedBytes)
	if err != nil {
		return
	}

	ans, err := json.Marshal(Response{
		Status:        "OK",
		Error:         err,
		DecodedString: decodedString,
	})
	if err != nil {
		return
	}

	_, err = w.Write(ans)
	if err != nil {
		return
	}
	return
}
