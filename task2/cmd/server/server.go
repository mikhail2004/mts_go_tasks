package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"golang.org/x/sync/errgroup"
	"mts_go_tasks/task2/internal/config"
	"mts_go_tasks/task2/internal/http-server/handlers"
	"net/http"
	"os/signal"
	"syscall"
)

const shutdownTimeout = 30

func ReadConfig() (cfg *config.Config, err error) {
	var configPath string
	flag.StringVar(&configPath, "c", ".config.yaml", "set config path")
	flag.Parse()
	cfg, err = config.NewConfig(configPath)
	if err != nil {
		return
	}
	return
}

func main() {
	cfg, err := ReadConfig()

	var (
		mux = http.NewServeMux()
		srv = &http.Server{
			Addr:    cfg.Address,
			Handler: mux,
		}
	)

	mux.HandleFunc("/version", handlers.GetAPIver)
	mux.HandleFunc("/decode", handlers.HandleDecodeRequest)
	mux.HandleFunc("/hard-op", handlers.ProcessHardRequest)

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	group, ctx := errgroup.WithContext(ctx)

	group.Go(func() (err error) {
		if err = srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			return fmt.Errorf("failed to serve http-server: %w", err)
		}
		return
	})

	group.Go(func() (err error) {
		<-ctx.Done()
		shutdownCtx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		err = srv.Shutdown(shutdownCtx)
		if err != nil {
			return
		}
		return
	})

	err = group.Wait()
	if err != nil {
		fmt.Printf("after wait: %s\n", err)
		return
	}

}
