package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type ResponseReport struct {
	StatusCode int
	Success    bool
}

type PostRequest struct {
	InputString string `json:"inputString"`
}

type Client struct {
	client       *http.Client
	responseUrls []string
}

func NewClient(client *http.Client, urls []string) Client {
	return Client{
		client:       client,
		responseUrls: urls,
	}
}

func (c *Client) AddUrl(url string) {
	c.responseUrls = append(c.responseUrls, url)
}

func (c *Client) SendRequests() {
	var err error = nil
	var rep ResponseReport
	for i := 0; err == nil; i++ {
		i = i % len(c.responseUrls)
		if i == 1 {
			err, rep = c.SendPostRequest(i)
		} else {
			err, rep = c.SendGetRequest(i)
		}
		if err != nil {
			fmt.Println(rep.Success, rep.StatusCode)
		}
	}
}

func (c *Client) SendPostRequest(ind int) (err error, rep ResponseReport) {
	req := PostRequest{InputString: "U3RyaW5n"}
	jsonReq, err := json.Marshal(req)
	if err != nil {
		return
	}
	resp, err := c.client.Post((c.responseUrls)[ind], "application/json", bytes.NewBuffer(jsonReq))
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusRequestTimeout
	}
	if err != nil {
		rep.Success = false
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func (c *Client) SendGetRequest(ind int) (err error, rep ResponseReport) {
	resp, err := c.client.Get((c.responseUrls)[ind])
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusRequestTimeout
	}
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func main() {
	urls := []string{
		"http://localhost:8080/version",
		"http://localhost:8080/decode",
		"http://localhost:8080/hard-op",
	}
	client := NewClient(&http.Client{Timeout: time.Duration(15) * time.Second}, urls)
	client.SendRequests()
}
